Installation
=============

Debian Buster 10
----------------

[Allegro Packages](https://packagecloud.io/allegro/ralph)  [Allegro Docs](https://ralph-ng.readthedocs.io/en/stable/installation/installation/)

Suggusted OS: Debian 10 Turnkey Core

_Trunkey Core Webmin can be accessed through https://\<ip-address\>:12321_

Ralph3 uses MySql 5.7.x as its database. You must install this version in order for Ralph to work. *This needs to be researched for an external database.*



#### Create Super User

Create superuser with the name 'ralph'.

    adduser <username> sudo  

Switch to primary user.

    su <username>  

#### Install Pre-Req's

Install python 3.6

    apt install -y python3.6

#### Install Repository

Allegro:

    curl -sL https://packagecloud.io/allegro/ralph/gpgkey | sudo apt-key add -

    sudo sh -c "echo 'deb https://packagecloud.io/allegro/ralph/ubuntu/ bionic main' >  /etc/apt/sources.list.d/ralph.list"

MySql:

Download mysql repository to the '/tmp' directory.

    wget -P /tmp https://dev.mysql.com/get/mysql-apt-config_0.8.22-1_all.deb

Install repository package and set up package.

    sudo dpkg -i /tmp/mysql-apt-config_0.8.22-1_all.deb

On the setup page select Mysql 5.7.x as default configuration.

#### Update Apt Repositories

    sudo apt update --allow-insecure-repositories

#### Install Packages

    sudo apt-get update && sudo apt-get install mysql-server nginx ralph-core

#### Setup MySql Databases

#### Setup Nginx Proxy Server

*Optional:* Clear current file content by using

    sudo bash -c 'echo "" > /etc/nginx/sites-available/default'

Basic nginx config `/etc/nginx/sites-available/default`.

    sudo nano /etc/nginx/sites-available/default

```bash
server {

    listen 80;
    client_max_body_size 512M;

    proxy_set_header Connection "";
    proxy_http_version 1.1;
    proxy_connect_timeout  300;
    proxy_read_timeout 300;

    access_log /var/log/nginx/ralph-access.log;
    error_log /var/log/nginx/ralph-error.log;

    location /static {
        alias /usr/share/ralph/static;
        access_log        off;
        log_not_found     off;
        expires 1M;
    }

    #location /media {
    #    alias /var/local/ralph/media;
    #    add_header Content-disposition "attachment";
    #}

    location / {
        proxy_pass http://127.0.0.1:8000;
        include /etc/nginx/uwsgi_params;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}
```

Restart nginx daemon

    sudo systemctl restart nginx.service


#### MySql Setup

Log into Mysql

    sudo mysql

Add user and database

    create user 'ralph_ng'@'127.0.0.1' identified by 'ralph_ng';
    grant all privileges on ralph_ng.* to 'ralph_ng'@'127.0.0.1';
    create database ralph_ng;
    FLUSH PRIVILEGES;

Quit

    QUIT;

Test database

    mysql -u ralph_ng -pralph_ng

    mysql > QUIT;

#### Ralph Configuration

If you choose to use custom database configuration and dont set it up with the inital prompt exit `/etc/ralph/conf.d/database.conf`
